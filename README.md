# XsollaProducts

Сервис реализован с помощью платформы ASP.NET Core 5 на языке C#

Для запуска потребуется:

- Любая IDE (например, Visual studio или JetBrains Rider) с рабочей нагрузкой ASP.NET
- Пакет SDK для .NET 5.0 или более поздней версии



Инструкция по запуску:
1. Открыть проект ProductManagement/ProductManagement.csproj
2. Собрать и запустить данный проект
3. В открывшемся окне браузера в Swagger'е протестировать CRUD методы

- **Создание товара:**

/api/ProductMarket/CreateNewProduct

- **Редактирование товара:**

/api/ProductMarket/UpdateProduct


- **Удаление товара по его идентификатору:**

/api/ProductMarket/DeleteProduct


-  **Получение информации о товаре по его идентификатору:**

/api/ProductMarket/GetProductById


- **Получение каталога товаров:**

/api/ProductMarket/GetAllProducts



В целях снижения нагрузки на сервис, реализовано кеширование.

**Предустановленные перед запуском приложения продукты описаны в ProductManagement/preinstalledProducts.json**
